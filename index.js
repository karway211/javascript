const root = document.getElementById('root');
const wrapper = document.createElement('div');
const bthMain = document.createElement('button');
const bthLog = document.createElement('button');
const bthReg = document.createElement('button');
const form = document.createElement('form');
const login = document.createElement('input');
const passw = document.createElement('input');
const signIn = document.createElement('button');
const confirm = document.createElement('input');
const save = document.createElement('button');
const reset = document.createElement('button');
const divButton = document.createElement('div');

root.classList.add('root-element');
bthReg.classList.add('bthReg');
bthLog.classList.add('bthLog');
wrapper.classList.add('wrapper-button');
bthMain.classList.add('bthMain');
form.classList.add('form');
login.classList.add('login');
passw.classList.add('passw');
confirm.classList.add('confirm');
save.classList.add('bth-save');
reset.classList.add('bth-reset');
divButton.classList.add('div-button');
signIn.classList.add('bth-sign-in')

bthMain.innerText = 'main';
bthLog.innerText = 'login';
bthReg.innerText = 'Register';
login.setAttribute('type', 'text');
login.setAttribute('placeholder', 'login');
passw.setAttribute('type', 'password');
passw.setAttribute('placeholder', 'password');
confirm.setAttribute('type', 'password');
confirm.setAttribute('placeholder', 'confirm password');
save.innerText = 'save';
reset.innerText = 'reset';
signIn.innerText = 'sign in'

divButton.appendChild(save);
divButton.appendChild(reset);
divButton.appendChild(signIn);
form.appendChild(login);
form.appendChild(passw);
form.appendChild(confirm);
form.appendChild(confirm);
form.appendChild(divButton);
wrapper.appendChild(bthMain);
wrapper.appendChild(bthLog);
root.appendChild(wrapper);
root.appendChild(bthReg);
root.appendChild(form);

function changeBthReg(){
    bthReg.style.display = 'none';
    form.style.display = 'flex';
}
bthReg.addEventListener('click', changeBthReg);

function changeBthLog(){
  bthReg.style.display = 'none';
  form.style.display = 'flex';
  confirm.style.display = 'none';
  save.style.display = 'none';
  reset.style.display = 'none';
  signIn.style.display = 'block';
  divButton.style.justifyContent = 'center';
}
bthLog.addEventListener('click', changeBthLog);






